package com.jie.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TTLMessageConfig  {
    /**
     * @description:交换机
     * @author: jie
     * @time: 2022/3/5 11:30
     */
    @Bean
    public DirectExchange ttlDirectExchange(){
        return new DirectExchange("ttl.direct");
    }
    /**
     * @description:队列
     * @author: jie
     * @time: 2022/3/5 11:30
     */
    @Bean
    public Queue ttlQueue(){
        //指定队列名称，并持久化
        return QueueBuilder.durable("ttl.queue")
                // 设置队列的超时时间，10秒
                .ttl(10000)
                // 指定死信交换机
                .deadLetterExchange("dl.direct")
                //设置routingKey
                .deadLetterRoutingKey("dl")
                .build();
    }
    /**
     * @description:将交换机和队列绑定
     * @author: jie
     * @time: 2022/3/5 11:30
     */
    @Bean
    public Binding ttlBinding(){
        return BindingBuilder.bind(ttlQueue()).to(ttlDirectExchange()).with("ttl");
    }
}
