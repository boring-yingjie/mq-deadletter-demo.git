package com.jie.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DelayedConfig {
    /**
     * @description:交换机
     * @author: jie
     * @time: 2022/3/5 15:29
     */
    @Bean
    public DirectExchange delayedExchange(){
        //指定交换机类型
        return ExchangeBuilder.directExchange("delay.direct")
                //设置delay属性为true
                .delayed()
                //持久化
                .durable(true)
                .build();
    }
    /**
     * @description:队列
     * @author: jie
     * @time: 2022/3/5 15:29
     */
    @Bean
    public Queue delayedQueue(){
        return new Queue("delay.queue");
    }
    /**
     * @description:绑定
     * @author: jie
     * @time: 2022/3/5 15:31
     */
    @Bean
    public Binding delayeBinding(){
        return BindingBuilder.bind(delayedQueue())
                .to(delayedExchange())
                .with("delay");
    }
}
