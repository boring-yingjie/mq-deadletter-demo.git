package com.jie.mq.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SpringRabbitListener {
    /**
     * @description:声明死信交换机、死信队列
     * @author: jie 
     * @time: 2022/3/5 10:30
     */
    @RabbitListener(bindings = @QueueBinding(
            //队列，持久化为true
            value = @Queue(name = "dl.queue",durable = "true"),
            //交换机
            exchange = @Exchange(name = "dl.direct"),
            //设置routingKey
            key = "dl"
    ))
    public void listenDlQueue(String msg){
        log.info("消费者接受到了dl.queue的延迟消息");
    }
    /**
     * @description:延迟交换机
     * @author: jie
     * @time: 2022/3/5 15:39
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "delay.queue",durable = "true"),
            exchange = @Exchange(name = "delay.direct",delayed = "true"),
            key = "delay"
    ))
    public void listenDelayExchange(String msg){
        log.info("消费者接受到了delay.queue的延迟消息:"+msg);
    }

}
